package no.noroff.main.exceptions;

public class InvalidArmorException extends ItemException{
    public InvalidArmorException(String message) {
        super(message);
    }
}
