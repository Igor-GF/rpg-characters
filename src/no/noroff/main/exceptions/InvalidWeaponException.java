package no.noroff.main.exceptions;

public class InvalidWeaponException extends ItemException{
    public InvalidWeaponException(String message) {
        super(message);
    }
}
