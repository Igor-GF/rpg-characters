package no.noroff.main.exceptions;

public class ItemException extends Exception {
    public ItemException(String message) {
        super(message);
    }
}
