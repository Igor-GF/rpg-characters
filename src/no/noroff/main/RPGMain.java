package no.noroff.main;
import no.noroff.main.character.*;
import no.noroff.main.exceptions.*;
import no.noroff.main.item.weapons.*;
import no.noroff.main.item.armor.*;

public class RPGMain {
    public static void main(String[] args) throws ItemException {
        // Creating characters
        Mage mage = new Mage("Helena");
        Ranger ranger = new Ranger("Marcela");
        Rogue rogue = new Rogue("Jan");
        Warrior warrior = new Warrior("Igor");

        // creating weapons
        Sword swordOfNature = new Sword("Sword of Nature", 3);
        Axe newAxe = new Axe("Axe of Caos", 3);
        Bow newBow = new Bow("Forest Bow", 3);
        Dagger newDagger = new Dagger("Golden Dagger", 3);
        Hammer newHammer = new Hammer("Hammer of the Gods", 3);
        Staff newStaff = new Staff("Gandalf's Staaff", 5);
        Wand newWand = new Wand("Foeksia's Wand", 3);

        // Creating Armor
        Cloth newCloth = new Cloth("Hobbit cloth", 1);
        Leather newLeather = new Leather("Elf leather", 1);
        Mail newMail = new Mail("Dwarf Mail", 1);
        Plate newPlate = new Plate("Knight Plate", 1);

        // increasing level of characters
        warrior.levelUp();
        warrior.levelUp();
        mage.levelUp();
        mage.levelUp();

        // printing created items
        System.out.println("========== Items: Weapons in the game ==========");
        System.out.println(swordOfNature);
        System.out.println(newAxe);
        System.out.println("========== Items: Armor in the game ==========");
        System.out.println(newCloth);
        System.out.println(newLeather);
        System.out.println(newMail);
        System.out.println(newPlate);

        // equiping characters
        System.out.println("========== Equipment session ==========");
        mage.equip(newStaff);
        ranger.equip(newBow);
        rogue.equip(newDagger);
        warrior.equip(newPlate);
        warrior.equip(newAxe);
        mage.equip(newCloth);
        ranger.equip(newLeather);
        rogue.equip(newCloth);

        // print all the characters in the game
        System.out.println("========== Charaters in the game ==========");
        System.out.println(mage);
        System.out.println(ranger);
        System.out.println(rogue);
        System.out.println(warrior);

        System.out.println("=================== end.===================");
    }
}
