package no.noroff.main.character;
import no.noroff.main.item.Item;
import no.noroff.main.item.Weapon;

public abstract class Hero {

    private String name;
    private int level;
    private PrimaryAttribute basePrimaryAttributes;
    private int totalPrimaryAttributes;
    protected Item[] items = new Item[4];

    public Hero(String name) {
        this.setName(name);
        this.setLevel(1);
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getLevel() {
        return this.level;
    }

    public void setLevel(int level) {
        this.level = level;
    }

    public int getTotalPrimaryAttributes() {
        return this.totalPrimaryAttributes;
    }

    public void setTotalPrimaryAttributes(int totalPrimaryAttributes) {
        this.totalPrimaryAttributes = totalPrimaryAttributes;
    }

    public void levelUpPlusAttibutes(int upStrength, int upDexterity, int upIntelligence, int upPrimary) {
        this.level++;
        this.basePrimaryAttributes.levelUpAttributes(upStrength, upDexterity, upIntelligence);
        this.totalPrimaryAttributes += upPrimary;
    }

    public PrimaryAttribute getBasePrimaryAttributes() {
        return basePrimaryAttributes;
    }

    public void setBasePrimaryAttributes(int strength, int dexterity, int intelligence) {
        this.basePrimaryAttributes = new PrimaryAttribute(strength, dexterity, intelligence);
    }

    public Item[] getItems() {
        return this.items;
    }

    public void setItems(Item[] items) {
        this.items = items;
    }

    public int calculateDPS() {
        if (this.items[3] == null) {
            return 1;
        }
        int weaponDPS = ((Weapon) this.items[3]).getDPS();
        return weaponDPS * (1 + totalPrimaryAttributes/100);
    }

    @Override
    public String toString()
    {
        return "Name: " + this.getName() +
                " | Level:" + this.getLevel() +
                " | S:" + this.getBasePrimaryAttributes().getStrength() +
                " | D:" + this.getBasePrimaryAttributes().getDexterity() +
                " | I:" + this.getBasePrimaryAttributes().getIntelligence() +
                " | DPS:" + this.calculateDPS();
    }
}
