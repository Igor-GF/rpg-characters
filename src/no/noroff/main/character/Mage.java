package no.noroff.main.character;
import no.noroff.main.exceptions.InvalidArmorException;
import no.noroff.main.exceptions.InvalidWeaponException;
import no.noroff.main.exceptions.ItemException;
import no.noroff.main.item.Armor;
import no.noroff.main.item.Item;
import no.noroff.main.item.armor.Cloth;
import no.noroff.main.item.weapons.Staff;
import no.noroff.main.item.weapons.Wand;

public class Mage extends Hero {

    public Mage(String name) {
        super(name);
        this.setBasePrimaryAttributes(1, 1, 8);
        this.setTotalPrimaryAttributes(8);
    }

    public void levelUp() {
        this.levelUpPlusAttibutes(1, 1, 5, 5);
    }

    public void equip(Item item) throws ItemException
    {
       try {
           if((item instanceof Staff) || (item instanceof Wand) || (item instanceof Cloth))
           {
               if (this.getLevel() >= item.getRequiredLevel()) {
                   Item[] itemsCopy = this.getItems();
                   itemsCopy[item.getSlot()] = item;
                   this.setItems(itemsCopy);
               } else {
                   System.out.println("This item requires level " + item.getRequiredLevel());
               }
           } else if (item instanceof Armor) {
               throw new InvalidArmorException("A Mage does not accept this armor.");
           } else {
               throw new InvalidWeaponException("A Mage cannot hold this weapon.");
           }
       }
       catch(Exception e) {
           System.out.println("Problem occured: " + e);
       }
    }
}
