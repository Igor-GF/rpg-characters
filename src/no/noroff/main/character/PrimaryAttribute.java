package no.noroff.main.character;

public class PrimaryAttribute {
    private int strength;
    private int dexterity;
    private int intelligence;

    public PrimaryAttribute(int strength, int dexterity, int intelligence) {
        this.setStrength(strength);
        this.setDexterity(dexterity);
        this.setIntelligence(intelligence);
    }

    public int getStrength() {
        return strength;
    }

    public void setStrength(int strength) {
        this.strength = strength;
    }

    public int getDexterity() {
        return dexterity;
    }

    public void setDexterity(int dexterity) {
        this.dexterity = dexterity;
    }

    public int getIntelligence() {
        return intelligence;
    }

    public void setIntelligence(int intelligence) {
        this.intelligence = intelligence;
    }

    public void levelUpAttributes(int upStrength, int upDexterity, int upIntelligence) {
        this.strength += upStrength;
        this.dexterity += upDexterity;
        this.intelligence += upIntelligence;
    }

    public int getArmor() {
        return 2;
    }

    public void setArmor(int armor) {
    }
}
