package no.noroff.main.character;
import no.noroff.main.exceptions.*;
import no.noroff.main.exceptions.ItemException;
import no.noroff.main.item.Armor;
import no.noroff.main.item.Item;
import no.noroff.main.item.armor.Mail;
import no.noroff.main.item.armor.Plate;
import no.noroff.main.item.weapons.*;

public class Warrior extends Hero{
    public Warrior(String name) {
        super(name);
        this.setBasePrimaryAttributes(5, 2, 1);
        this.setTotalPrimaryAttributes(5);
    }

    public void levelUp() {
        this.levelUpPlusAttibutes(3, 2, 1, 3);
    }

    public void equip(Item item) throws ItemException
    {
        try {
            if ((item instanceof Axe) || (item instanceof Hammer) || (item instanceof Sword) || (item instanceof Mail) || (item instanceof Plate)) {
                if (this.getLevel() >= item.getRequiredLevel()) {
                    Item[] itemsCopy = this.getItems();
                    itemsCopy[item.getSlot()] = item;
                    this.setItems(itemsCopy);
                } else {
                    System.out.println("This item requires level " + item.getRequiredLevel());
                }
            } else if (item instanceof Armor) {
                throw new InvalidArmorException("A Warrior does not accept this armor.");
            }else {
                throw new InvalidWeaponException("A Warrior cannot hold this weapon.");
            }
        }
        catch(Exception e) {
            System.out.println("Problem occured: " + e);
        }
    }
}
