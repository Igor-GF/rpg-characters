package no.noroff.main.character;
import no.noroff.main.exceptions.InvalidArmorException;
import no.noroff.main.exceptions.InvalidWeaponException;
import no.noroff.main.exceptions.ItemException;
import no.noroff.main.item.Armor;
import no.noroff.main.item.Item;
import no.noroff.main.item.armor.Leather;
import no.noroff.main.item.armor.Mail;
import no.noroff.main.item.weapons.*;

public class Ranger extends Hero {

    public Ranger(String name) {
        super(name);
        this.setBasePrimaryAttributes(1, 7, 1);
        this.setTotalPrimaryAttributes(7);
    }

    public void levelUp() {
        this.levelUpPlusAttibutes(1, 5, 1, 5);
    }

    public void equip(Item item) throws ItemException
    {
        try {
            if ((item instanceof Bow) || (item instanceof Leather) || (item instanceof Mail)) {
                if (this.getLevel() >= item.getRequiredLevel()) {
                    Item[] itemsCopy = this.getItems();
                    itemsCopy[item.getSlot()] = item;
                    this.setItems(itemsCopy);
                } else {
                    System.out.println("This item requires level " + item.getRequiredLevel());
                }
            } else if (item instanceof Armor) {
                throw new InvalidArmorException("A Ranger does not accept this armor.");
            } else {
                throw new InvalidWeaponException("A Ranger cannot hold this weapon.");
            }
        }
        catch(Exception e) {
            System.out.println("Problem occured: " + e);
        }
    }
}
