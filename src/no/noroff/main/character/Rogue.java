package no.noroff.main.character;
import no.noroff.main.exceptions.InvalidArmorException;
import no.noroff.main.exceptions.InvalidWeaponException;
import no.noroff.main.exceptions.ItemException;
import no.noroff.main.item.Armor;
import no.noroff.main.item.Item;
import no.noroff.main.item.armor.Leather;
import no.noroff.main.item.armor.Mail;
import no.noroff.main.item.weapons.*;

public class Rogue extends Hero {
    public Rogue(String name) {
        super(name);
        this.setBasePrimaryAttributes(2, 6, 1);
        this.setTotalPrimaryAttributes(6);
    }

    public void levelUp() {
        this.levelUpPlusAttibutes(1, 4, 1, 4);
    }

    public void equip(Item item) throws ItemException
    {
        try {
            if ((item instanceof Dagger) || (item instanceof Sword) || (item instanceof Leather) || (item instanceof Mail)) {
                if (this.getLevel() >= item.getRequiredLevel()) {
                    Item[] itemsCopy = this.getItems();
                    itemsCopy[item.getSlot()] = item;
                    this.setItems(itemsCopy);
                } else {
                    System.out.println("This item requires level " + item.getRequiredLevel());
                }
            } else if (item instanceof Armor) {
                throw new InvalidArmorException("A Rogue does not accept this armor.");
            } else {
                throw new InvalidWeaponException("A Rogue cannot hold this weapon.");
            }
        }
        catch(Exception e) {
            System.out.println("Problem occured: " + e);
        }
    }
}
