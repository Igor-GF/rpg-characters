package no.noroff.main.item;

public class Armor extends Item {
    private int defense;
    private String type;

    public Armor(String name, int requiredLevel) {
        super(name, requiredLevel);
        this.setSlot(1);
    }

    public void setDefense(int defense) {
        this.defense = defense;
    }

    public int getDefense() {
        return this.defense;
    }

    public String getType() {
        return this.type;
    }

    public void setType(String type) {
        this.type = type;
    }

    @Override
    public String toString() {
        return  "name='" + this.getName() + '\'' +
                ", requiredLevel=" + this.getRequiredLevel() + '\'' +
                ", slot=" + this.getSlot() + '\'' +
                ", type:" + this.getType()  + '\'' +
                ", defense:" + this.getDefense() + '\'';
    }
}
