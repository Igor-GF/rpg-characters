package no.noroff.main.item.weapons;
import no.noroff.main.item.Weapon;

public class Sword extends Weapon {

    public Sword(String name, int requiredLevel) {
        super(name, requiredLevel);
        this.setType("SWORD");
        this.setDamage(5);
        this.setAttackSpeed(3);
    }
}
