package no.noroff.main.item.weapons;
import no.noroff.main.item.*;

public class Bow extends Weapon {

    public Bow(String name, int requiredLevel) {
        super(name, requiredLevel);
        this.setType("BOW");
        this.setDamage(4);
        this.setAttackSpeed(2);
    }
}
