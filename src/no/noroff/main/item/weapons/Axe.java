package no.noroff.main.item.weapons;
import no.noroff.main.item.Weapon;

public class Axe extends Weapon {

    public Axe(String name, int requiredLevel) {
        super(name, requiredLevel);
        this.setType("AXE");
        this.setDamage(9);
        this.setAttackSpeed(1);
    }
}