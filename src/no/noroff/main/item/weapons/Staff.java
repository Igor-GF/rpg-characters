package no.noroff.main.item.weapons;
import no.noroff.main.item.*;

public class Staff extends Weapon {

    public Staff(String name, int requiredLevel) {
        super(name, requiredLevel);
        this.setType("STAFF");
        this.setDamage(4);
        this.setAttackSpeed(5);
    }
}