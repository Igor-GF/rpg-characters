package no.noroff.main.item.weapons;
import no.noroff.main.item.*;

public class Hammer extends Weapon {

    public Hammer(String name, int requiredLevel) {
        super(name, requiredLevel);
        this.setType("HAMMER");
        this.setDamage(12);
        this.setAttackSpeed(1);
    }
}