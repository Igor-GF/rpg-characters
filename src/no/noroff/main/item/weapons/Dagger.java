package no.noroff.main.item.weapons;
import no.noroff.main.item.*;

public class Dagger extends Weapon {

    public Dagger(String name, int requiredLevel) {
        super(name, requiredLevel);
        this.setType("DAGGER");
        this.setDamage(2);
        this.setAttackSpeed(8);
    }
}