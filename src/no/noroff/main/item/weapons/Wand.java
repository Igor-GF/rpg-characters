package no.noroff.main.item.weapons;
import no.noroff.main.item.*;

public class Wand extends Weapon {

    public Wand(String name, int requiredLevel) {
        super(name, requiredLevel);
        this.setType("WAND");
        this.setDamage(3);
        this.setAttackSpeed(7);
    }
}