package no.noroff.main.item;

public abstract class Weapon extends Item{
    private int damage;
    private int attackSpeed;
    private String type;

    public Weapon(String name, int requiredLevel) {
        super(name, requiredLevel);
        this.setSlot(3);
    }

    public void setDamage(int damage) {
        this.damage = damage;
    }

    public String getType() {
        return this.type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public int getAttackSpeed() {
        return attackSpeed;
    }

    public void setAttackSpeed(int attackSpeed) {
        this.attackSpeed = attackSpeed;
    }

    public int getDPS() {
        return this.attackSpeed * this.damage;
    }

    @Override
    public String toString() {
        return  "name='" + this.getName() + '\'' +
                ", requiredLevel=" + this.getRequiredLevel() + '\'' +
                ", slot=" + this.getSlot() + '\'' +
                ", type:" + this.getType()  + '\'' +
                ", attack speed:" + this.getAttackSpeed() + '\'' +
                ", DPS:" + this.getDPS();
    }
}
