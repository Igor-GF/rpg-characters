package no.noroff.main.item.armor;
import no.noroff.main.item.Armor;

public class Mail extends Armor {
    public Mail(String name, int requiredLevel) {
        super(name, requiredLevel);
        this.setType("MAIL");
        this.setDefense(5);
    }
}
