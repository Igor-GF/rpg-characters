package no.noroff.main.item.armor;
import no.noroff.main.item.Armor;

public class Leather extends Armor {
    public Leather(String name, int requiredLevel) {
        super(name, requiredLevel);
        this.setType("LEATHER");
        this.setDefense(3);
    }
}
