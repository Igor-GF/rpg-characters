package no.noroff.main.item.armor;
import no.noroff.main.item.Armor;

public class Plate extends Armor {
    public Plate(String name, int requiredLevel) {
        super(name, requiredLevel);
        this.setType("PLATE");
        this.setDefense(8);
    }
}
