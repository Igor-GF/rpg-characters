package no.noroff.main.item.armor;
import no.noroff.main.item.Armor;

public class Cloth extends Armor {
    public Cloth(String name, int requiredLevel) {
        super(name, requiredLevel);
        this.setType("CLOTH");
        this.setDefense(1);
    }
}
