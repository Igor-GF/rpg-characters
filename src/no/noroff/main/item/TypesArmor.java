package no.noroff.main.item;

public enum TypesArmor {
    CLOTH,      // 0
    LEATHER,    // 1
    MAIL,       // 2
    PLATE       // 3
}
