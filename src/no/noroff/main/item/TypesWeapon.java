package no.noroff.main.item;

public enum TypesWeapon {
    AXE,    // 0
    BOW,    // 1
    DAGGER, // 2
    HAMMER, // 3
    STAFF,  // 4
    SWORD,  // 5
    WAND    // 6
}
