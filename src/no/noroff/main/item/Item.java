package no.noroff.main.item;

public abstract class Item {
    private String name;
    private int requiredLevel;
    private int slot;

    public Item(String name, int requiredLevel) {
        this.setName(name);
        this.setRequiredLevel(requiredLevel);
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getRequiredLevel() {
        return requiredLevel;
    }

    public void setRequiredLevel(int requiredLevel) {
        this.requiredLevel = requiredLevel;
    }

    public int getSlot() {
        return slot;
    }

    public void setSlot(int slot) {
        this.slot = slot;
    }

}
