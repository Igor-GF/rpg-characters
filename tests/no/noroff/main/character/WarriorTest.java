package no.noroff.main.character;
import no.noroff.main.exceptions.ItemException;
import no.noroff.main.item.Weapon;
import no.noroff.main.item.weapons.Sword;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

class WarriorTest {
    Warrior warrior;

    @BeforeEach
    public void setUp() {
        warrior = new Warrior("warrior_name");
    }

    @Test
    public void levelUp_attributesGetCorrectValues_shouldReturnCorrectValues() {
        PrimaryAttribute expected = new PrimaryAttribute(8,4,2);
        warrior.levelUp();
        PrimaryAttribute actual = warrior.getBasePrimaryAttributes();
        assertEquals(expected.getDexterity(), actual.getDexterity());
        assertEquals(expected.getIntelligence(), actual.getIntelligence());
        assertEquals(expected.getStrength(), actual.getStrength());
    }

    @Test
    public void equip_giveRightWeaponHigherLevelRequired_testShouldNotPass() throws ItemException {
        Weapon sword = new Sword("sword_name", 2);
        Weapon expected = null;
        warrior.equip(sword);
        Weapon actual = warrior.getItems()[0];
        assertEquals(expected, actual);
    }
}