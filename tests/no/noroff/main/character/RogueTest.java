package no.noroff.main.character;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

class RogueTest {
    Rogue rogue;

    @BeforeEach
    public void setUp() {
        rogue = new Rogue("rogue_name");
    }

    @Test
    public void levelUp_attributesGetCorrectValues_shouldReturnCorrectValues() {
        PrimaryAttribute expected = new PrimaryAttribute(3,10,2);
        rogue.levelUp();
        PrimaryAttribute actual = rogue.getBasePrimaryAttributes();
        assertEquals(expected.getDexterity(), actual.getDexterity());
        assertEquals(expected.getIntelligence(), actual.getIntelligence());
        assertEquals(expected.getStrength(), actual.getStrength());
    }
}