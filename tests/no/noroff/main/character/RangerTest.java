package no.noroff.main.character;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class RangerTest {
    Ranger ranger;

    @BeforeEach
    public void setUp() {
        ranger = new Ranger("ranger_name");
    }

    @Test
    public void levelUp_attributesGetCorrectValues_shouldReturnCorrectValues() {
        PrimaryAttribute expected = new PrimaryAttribute(2,12,2);
        ranger.levelUp();
        PrimaryAttribute actual = ranger.getBasePrimaryAttributes();
        assertEquals(expected.getDexterity(), actual.getDexterity());
        assertEquals(expected.getIntelligence(), actual.getIntelligence());
        assertEquals(expected.getStrength(), actual.getStrength());
    }
}