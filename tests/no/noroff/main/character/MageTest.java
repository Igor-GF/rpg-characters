package no.noroff.main.character;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class MageTest {
    Mage mage;

    @BeforeEach
    public void setUp() {
        mage = new Mage("mage_name");
    }

    @Test
    public void levelUp_attributesGetCorrectValues_shouldReturnCorrectValues() {
        PrimaryAttribute expected = new PrimaryAttribute(2,2,13);
        mage.levelUp();
        PrimaryAttribute actual = mage.getBasePrimaryAttributes();
        assertEquals(expected.getDexterity(), actual.getDexterity());
        assertEquals(expected.getIntelligence(), actual.getIntelligence());
        assertEquals(expected.getStrength(), actual.getStrength());
    }
}