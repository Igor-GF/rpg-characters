package no.noroff.main.character;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.assertEquals;

class HeroTest {
    Mage mage;
    Ranger ranger;
    Rogue rogue;
    Warrior warrior;

    @BeforeEach
    public void setUp() {
        mage = new Mage("mage_name");
        ranger = new Ranger("ranger_name");
        rogue = new Rogue("rogue_name");
        warrior = new Warrior("warrior_name");
    }

    @Test
    public void getLevel_characterLevelStartsWithOne_shouldReturnOne() {
        // Arrange
        int expected = 1;
        // Act
        int actual = mage.getLevel();
        // Assert
        assertEquals(expected, actual);
    }

    @Test
    public void levelUp_characterLevelTunsIntoTwo_shouldReturnTwo() {
        int expected = 2;
        mage.levelUp();
        int actual = mage.getLevel();
        assertEquals(expected, actual);
    }

    @Test
    public void magePrimaryAttributes_mageStartsWithCorrectAttributes_shouldReturnCorrectValues() {
        PrimaryAttribute expected = new PrimaryAttribute(1,1,8);
        PrimaryAttribute actual = mage.getBasePrimaryAttributes();
        assertEquals(expected.getDexterity(), actual.getDexterity());
        assertEquals(expected.getIntelligence(), actual.getIntelligence());
        assertEquals(expected.getStrength(), actual.getStrength());
    }

    @Test
    public void rangerPrimaryAttributes_mageStartsWithCorrectAttributes_shouldReturnCorrectValues() {
        PrimaryAttribute expected = new PrimaryAttribute(1,7,1);
        PrimaryAttribute actual = ranger.getBasePrimaryAttributes();
        assertEquals(expected.getDexterity(), actual.getDexterity());
        assertEquals(expected.getIntelligence(), actual.getIntelligence());
        assertEquals(expected.getStrength(), actual.getStrength());
    }

    @Test
    public void roguePrimaryAttributes_mageStartsWithCorrectAttributes_shouldReturnCorrectValues() {
        PrimaryAttribute expected = new PrimaryAttribute(2,6,1);
        PrimaryAttribute actual = rogue.getBasePrimaryAttributes();
        assertEquals(expected.getDexterity(), actual.getDexterity());
        assertEquals(expected.getIntelligence(), actual.getIntelligence());
        assertEquals(expected.getStrength(), actual.getStrength());
    }

    @Test
    public void warriorPrimaryAttributes_mageStartsWithCorrectAttributes_shouldReturnCorrectValues() {
        PrimaryAttribute expected = new PrimaryAttribute(5,2,1);
        PrimaryAttribute actual = warrior.getBasePrimaryAttributes();
        assertEquals(expected.getDexterity(), actual.getDexterity());
        assertEquals(expected.getIntelligence(), actual.getIntelligence());
        assertEquals(expected.getStrength(), actual.getStrength());
    }
}