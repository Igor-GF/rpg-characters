# RPG Characters

The assignment is the weekly project of the Noroff Java Full Stack traineeship.
It consists in RPG Character application where the user can create their own characters based on different classes.
User can: create characters, level up them, and also create weapons and items to equip the characters.

# Technologies
Java

## Getting started

To run the application is it probably better to use an IDE, such as IntelliJ (which this application was built in) with JDK version 17 kit installed.
Open the project from the IDE and run the PRGMain.java file.

Create character:  Class varName = new Class("Character_name");
    Classes: Mage, Ranger, Rogue, Warrior.
    Example: Ranger legolas = new Ranger("Legolas");

Level up:  instanceOfCharacter.levelUp();
    Example: legolas.levelUp();

Create weapon: WeaponType varName = new WeaponType("Weapon_name", "level_required");
    Weapon types: Axe, Bow, Dagger, Hammer, Staff, Sword, Wand.
    Example: Bow silverBow = new Bow("Silver Bow", 3);

Create armor: ArmorType varName = new ArmorType("Weapon_name", "level_required");
    Armor types: Cloth, Leather, Mail, Plate.
    Example: Cloth darkMail = new Cloth("Dark mail", 5);

Equip character:  instanceOfCharacter.equip(instanceOfItem);
    Example: legolas.equip(darkMail);
             legolas.equip(silverBow);

## Contributing
I am open to receive advice and tips which can help me to improve the OOP design, the logic, to clean up the code, etc. So that I can improve myself as a developer.

## Authors and acknowledgment
https://gitlab.com/muskatel  who has helped me by showing the logic of connecting the Items to the Character.
https://github.com/iljaasdhonre  who has given me info about working with Enum.

## Project status
Project for exclusively study purposes.